@extends('layouts.template')

@section('content')
<div class="page-header login-page header-filter" filter-color="black" style="background-image: url('{{url('assets')}}/img/login.jpg'); background-size: cover; background-position: top center;">
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
                    <div class="col-md-5 p-lg-5 mx-auto my-5">
                      <h1 class="display-1 font-weight-normal text-dark"><strong>Hello &#10084;</strong></h1>
                      <p class="lead font-weight-normal text-dark">{!!date('H:i:s');!!}</p>
                      <a href="https://api.whatsapp.com/send?phone=6285888293705&text=Hai, ini shidqi!" class="btn btn-outline-secondary">Coming soon</a>
                    </div>
                    <div>
                        <?php
                                $tgl1 = new DateTime("2020-04-10");
                                $tgl2 = new DateTime("2020-12-28");
                                $d = $tgl2->diff($tgl1)->days + 1;
                                echo $d." hari sejak pandemi berlangsung";
                            ?>
                            <p>Stay Safe Guys &#10084;</p>
                    </div>
                    <div class="product-device box-shadow d-none d-md-block"></div>
                    <div class="product-device product-device-2 box-shadow d-none d-md-block"></div>
                  </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
