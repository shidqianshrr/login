<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{
    function index()
    {
    $data['page'] = 'Dashboard';
    return view('dashboard')->with($data);
    }
}
