<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class MasukController extends Controller
{
    function index()
    {
        $data['page'] = 'Masuk';
        return view('masuk')->with($data);
    }

    public function auth(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password])){
            return "hai". Auth::user()->name;
        }
        else {
            return "Sorry";
        }
    }
}
