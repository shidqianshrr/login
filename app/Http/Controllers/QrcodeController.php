<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Generator;

class QrcodeController extends Controller
{
    function index()
    {
        $qrcode = new Generator;
        $qr = $qrcode->size(100)
                ->generate('Tes');
        return view('qrcode');
    }
}
