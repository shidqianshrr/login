<?php

namespace App\Models\Net_HRD;

use Illuminate\Database\Eloquent\Model;

class DepartementModel extends Model
{
    protected $connection = "net_hrd";
    protected $table = "departement";
    public $timestamps = false;
}
