<?php

namespace App\Models\Net_HRD;

use Illuminate\Database\Eloquent\Model;

class EmployeesModel extends Model
{
    protected $connection = "net_hrd";
    protected $table = "employees";
    public $timestamps = false;

    public function departement()
    {
        return $this->hasOne('App\Models\Net_HRD\DepartementModel', 'id', 'departement_id');
    }

}
